package com.payroll.unitTest.businessLogic;

import com.payroll.businessLogic.BusinessCalendar;
import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.businessLogic.TimeCard;
import com.payroll.businessLogic.TimeUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class HourlyEmployeeTest {
    private BusinessCalendar bCalendar;

    @Before
    public void setup() {
        final List<Date> holidays = new ArrayList<Date>();
        holidays.add(TimeUtils.buildDate(2018, 12, 1));
        // build business calendar with holidays list above

        this.bCalendar = new BusinessCalendar() {
            public boolean isNationalHoliday(Date date) {
                for (Date tdate: holidays){
                    if (TimeUtils.isSameDay(tdate, date)) return true;
                }
                return false;
            }
        };
    }

    @Test
    public void testMorningOnly() {
        // Arrange
        HourlyEmployee johnDoe = new HourlyEmployee("Bob", 120);
        johnDoe.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/27 09:00:00"), TimeUtils.buildDate("2018/11/27 12:00:00")));

        // Act
        double salary = johnDoe.calculatePay();

        // Assert
        double expectedSalary = 360;
        assertEquals(expectedSalary, salary,0.001);
    }

    @Test
    public void testFullDayNoOTOnly() {
        // Arrange
        HourlyEmployee johnDoe = new HourlyEmployee("Bob", 120);
        johnDoe.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/27 09:00:00"), TimeUtils.buildDate("2018/11/27 18:00:00")));

        // Act
        double salary = johnDoe.calculatePay();

        // Assert
        double expectedSalary = 960;
        assertEquals(expectedSalary, salary,0.001);
    }

    @Test
    public void testFullDayWithOT() {
        // Arrange
        HourlyEmployee johnDoe = new HourlyEmployee("Bob", 120);
        johnDoe.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/11/27 09:00:00"), TimeUtils.buildDate("2018/11/27 20:00:00")));

        // Act
        double salary = johnDoe.calculatePay();

        // Assert
        double expectedSalary = 1320;
        assertEquals(expectedSalary, salary,0.001);
    }

    @Test
    public void testWorkingOnHoliday() {
        // Arrange
        HourlyEmployee johnDoe = new HourlyEmployee("Bob", 120);
        johnDoe.addTimeCard(new TimeCard(TimeUtils.buildDate("2018/12/01 09:00:00"), TimeUtils.buildDate("2018/12/01 12:00:00")));

        // Act
        double salary = johnDoe.calculatePay();

        // Assert
        double expectedSalary = 540;
        assertEquals(expectedSalary, salary,0.001);
    }
}
